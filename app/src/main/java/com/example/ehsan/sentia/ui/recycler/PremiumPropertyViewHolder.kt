package com.example.ehsan.sentia.ui.recycler

import android.view.View
import com.example.ehsan.sentia.R
import com.example.ehsan.sentia.model.entities.Listings
import com.example.ehsan.sentia.utils.Utils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_property.view.*

class PremiumPropertyViewHolder(itemView:View) : GeneralViewHolder(itemView) {

    //ToDo change the input name
    override fun configureWith(listings: Listings){
        itemView.propertyTitle.text = listings.Area

        //ToDo Which url?
        Utils.loadImageEfficiently(itemView, listings.ImageUrls[2])
        Picasso.get().load(listings.owner.image.medium.url).resizeDimen(R.dimen.owner_image, R.dimen.owner_image).into(itemView.ownerImage)

        itemView.bedCount.text = listings.Bedrooms.toString()
        itemView.bathCount.text = listings.Bathrooms.toString()
        itemView.carCount.text = listings.Carspaces.toString()
        itemView.propertyAddress.text = listings.Location.Address
        itemView.ownerName.text = listings.owner.name + " " + listings.owner.lastName
    }
}