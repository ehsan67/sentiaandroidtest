package com.example.ehsan.sentia.utils

import android.util.Log
import android.view.ViewTreeObserver
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_property.view.*


object Utils {
    fun loadImageEfficiently(item: View, url: String) {
        var finalHeight: Int
        var finalWidth = 0

        val vto = item.getViewTreeObserver()
        vto.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                item.getViewTreeObserver().removeOnPreDrawListener(this)
                finalHeight = item.getMeasuredHeight()
                finalWidth = item.getMeasuredWidth()
                Picasso.get().load(url).resize(finalWidth, 0).into(item.propertyImage)

                return true
            }
        })
    }
}