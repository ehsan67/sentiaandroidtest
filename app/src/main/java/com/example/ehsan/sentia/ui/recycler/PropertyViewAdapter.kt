package com.example.ehsan.sentia.ui.recycler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ehsan.sentia.R
import com.example.ehsan.sentia.model.entities.Listings
import com.example.ehsan.sentia.utils.disposedBy
import com.jakewharton.rxrelay2.BehaviorRelay
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

typealias ItemClickedLambda = (v: View, position: Int) -> Unit

class PropertyViewAdapter(var onItemClicked: ItemClickedLambda) : RecyclerView.Adapter<GeneralViewHolder>() {
    private val bag = CompositeDisposable()
    internal val properties = BehaviorRelay.createDefault(listOf<Listings>())

    init {
        properties.observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    notifyDataSetChanged()
                }.disposedBy(bag)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GeneralViewHolder {
        val view: View
        val viewHolder: GeneralViewHolder
        when (viewType) {
            0 -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_property, parent, false)
                viewHolder = PropertyViewHolder(view)
            }
            else -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_premium_property, parent, false)
                viewHolder = PremiumPropertyViewHolder(view)
            }
        }

        view.setOnClickListener { v ->
            onItemClicked(v, viewHolder.adapterPosition)
        }

        return viewHolder
    }

    override fun getItemCount(): Int {
        return properties.value.size
    }

    override fun onBindViewHolder(holder: GeneralViewHolder, position: Int) {
        holder.configureWith(properties.value[position])
    }


    override fun getItemViewType(position: Int): Int {
        return properties.value[position].is_premium
    }


}