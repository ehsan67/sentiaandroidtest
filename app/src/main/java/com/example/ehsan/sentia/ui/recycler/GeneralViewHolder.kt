package com.example.ehsan.sentia.ui.recycler

import android.support.v7.widget.RecyclerView
import android.view.View
import com.example.ehsan.sentia.model.entities.Listings

abstract class GeneralViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun configureWith(listings: Listings)
}