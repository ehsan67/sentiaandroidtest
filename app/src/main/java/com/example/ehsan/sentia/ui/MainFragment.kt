package com.example.ehsan.sentia.ui

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.ehsan.sentia.R
import com.example.ehsan.sentia.model.entities.Listings
import com.example.ehsan.sentia.ui.recycler.PropertyViewAdapter
import com.example.ehsan.sentia.utils.disposedBy
import com.example.ehsan.sentia.viewmodel.PropertyListViewModel
import com.example.ehsan.sentia.viewmodel.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_main.view.*
import kotlinx.coroutines.experimental.launch

class MainFragment : Fragment() {

    private lateinit var mPropertyViewAdapter: PropertyViewAdapter
    private lateinit var mViewModel: PropertyListViewModel
    private val bag = CompositeDisposable()
    private lateinit var mRootView: View
    private var mListings: List<Listings>? = null
    private var mIsNewState = true

    var mCallback: OnPropertySelectedListener? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true;

        mViewModel = ViewModelProviders.of(this, ViewModelFactory()).get(PropertyListViewModel::class.java)

        launch {
            loadData()
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mIsNewState = savedInstanceState == null

        mRootView = inflater.inflate(R.layout.fragment_main, container, false)
        attachUI()
        return mRootView
    }

    private fun loadData() {
        mViewModel.getProperties().observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { properties ->
                            mListings = properties.data.listings
                            loadPropertyList()
                        },
                        { error ->
                            showProgressbar(false)
                            Toast.makeText(context, error.message, Toast.LENGTH_LONG).show()
                            Log.i("DevelopTest", error.message) })
                .disposedBy(bag)
    }

    private fun loadPropertyList() {
        showProgressbar(false)
        mListings?.let {
            mPropertyViewAdapter.properties.accept(it)
        }
    }


    private fun attachUI() {
        val linearLayoutManager = LinearLayoutManager(context)
        val dividerItemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)

        mRootView.propertyRecyclerView.layoutManager = linearLayoutManager
        mRootView.propertyRecyclerView.setHasFixedSize(true)
        mRootView.propertyRecyclerView.addItemDecoration(dividerItemDecoration)

        initializeListView()
    }

    private fun initializeListView() {
        mPropertyViewAdapter = PropertyViewAdapter{view, position -> rowTapped(position)}
        mRootView.propertyRecyclerView.adapter = mPropertyViewAdapter

        if (!mIsNewState) {
            loadPropertyList()
        }

    }

    private fun rowTapped(position: Int) {
        mCallback?.onPropertySelected(mListings?.get(position)?.Id)
    }

    private fun showProgressbar(show : Boolean){
        if (show){
            mRootView.progressBar.visibility = View.VISIBLE
            mRootView.propertyRecyclerView.visibility = View.GONE
        }else{
            mRootView.progressBar.visibility = View.GONE
            mRootView.propertyRecyclerView.visibility = View.VISIBLE
        }
    }

    interface OnPropertySelectedListener {
        fun onPropertySelected(propertyId: String?)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is MainActivity){
            mCallback = context
        }
    }

    override fun onDestroy() {
        bag.clear()
        super.onDestroy()
    }
}