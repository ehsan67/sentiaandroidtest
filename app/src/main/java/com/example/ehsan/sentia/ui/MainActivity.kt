package com.example.ehsan.sentia.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.ehsan.sentia.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainFragment.OnPropertySelectedListener {

    override fun onPropertySelected(propertyId: String?) {
        val detailsFragment = supportFragmentManager.findFragmentByTag("DetailsTAG")
        if (detailsFragment is DetailsFragment) {
            detailsFragment.updatePropertyId(propertyId ?: "")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {

            val mainFragment = MainFragment()
            supportFragmentManager.beginTransaction().add(R.id.main_fragment_container, mainFragment, "MainTAG")
                    .commit()

            details_fragment_container?.let {
                val detailsFragment = DetailsFragment()
                supportFragmentManager.beginTransaction().add(R.id.details_fragment_container, detailsFragment, "DetailsTAG")
                        .commit()
            }
        }
    }
}