package com.example.ehsan.sentia.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ehsan.sentia.R
import kotlinx.android.synthetic.main.fragment_details.view.*

class DetailsFragment : Fragment() {
    private lateinit var mRootView: View
    private var mPropertyId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mRootView = inflater.inflate(R.layout.fragment_details, container, false)
        updatePropertyId(mPropertyId)
        return mRootView
    }

    fun updatePropertyId(id: String) {
        mPropertyId = id
        mRootView.propertyId.text = id
    }
}