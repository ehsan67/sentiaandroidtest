package com.example.ehsan.sentia.model.network.endpoints

import com.example.ehsan.sentia.model.entities.Properties
import io.reactivex.Single
import retrofit2.http.GET

interface JsonPlaceHolder {

    @GET("properties")
    fun getProperties() : Single<Properties>

}