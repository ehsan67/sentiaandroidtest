package com.example.ehsan.sentia.model

import com.example.ehsan.sentia.model.entities.Properties
import io.reactivex.Single

class ModelLayer{
    companion object {
        val shared = ModelLayer()
    }

    private val networkLayer = NetworkLayer.instance

    fun getProperties(): Single<Properties> {
        return networkLayer.getProperties()
    }

}