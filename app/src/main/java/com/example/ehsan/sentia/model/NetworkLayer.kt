package com.example.ehsan.sentia.model

import com.example.ehsan.sentia.model.entities.Properties
import com.example.ehsan.sentia.model.network.endpoints.JsonPlaceHolder
import com.example.ehsan.sentia.model.network.helpers.ServiceGenerator
import io.reactivex.Single

class NetworkLayer {
    companion object { val instance = NetworkLayer() }


    private val placeHolderApi: JsonPlaceHolder

    init {
        placeHolderApi = ServiceGenerator.createService(JsonPlaceHolder::class.java)
    }

    fun getProperties(): Single<Properties> {
        return placeHolderApi.getProperties()
    }


}