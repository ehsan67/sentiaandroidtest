package com.example.ehsan.sentia.viewmodel

import android.arch.lifecycle.ViewModel
import com.example.ehsan.sentia.model.ModelLayer
import com.example.ehsan.sentia.model.entities.Properties
import io.reactivex.Single

class PropertyListViewModel : ViewModel(){

    fun getProperties(): Single<Properties> {
        return ModelLayer.shared.getProperties()
    }

}